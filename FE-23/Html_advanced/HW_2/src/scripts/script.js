const btn = document.querySelector(".header__btn-burger");
const btnExit = document.querySelector(".header__btn-exit");
const nav = document.querySelector(".header__nav");
function checkScreenWidth() {

    if (window.innerWidth > 468) {
        btn.style.display = 'none';
        btnExit.style.display = 'none';
    } else{
        btn.style.display = 'flex';
    btn.addEventListener("click", ()=> {
            btn.style.display = "none";
            btnExit.style.display = "flex";
            nav.style.display = "block";
    })
    btnExit.addEventListener("click", ()=> {
        btn.style.display = "flex";
        btnExit.style.display = "none";
        nav.style.display = "none";
    })
    }
}

window.addEventListener('load', checkScreenWidth);
window.addEventListener('resize', checkScreenWidth);
