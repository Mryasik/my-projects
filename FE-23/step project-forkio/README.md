<h1 align="center">Step-project-forkio</h1>

## List of technologies used

- html
- css
- scss
- js
- gulp


## Development team
<span>Viktoria Shvets</span>
<span>Yaroslav </span>


## Tasks completed by each member

1. Viktoria Shvets:
   - Create the site HEADER with the top menu (including the drop-down menu at low screen resolution).
   - Create section "People Are Talking About Fork".

2. Yaroslav :
   - Create block "Revolutionary Editor".
   - Create section "Here is what you get".
   - Create section "Fork Subscription Pricing".