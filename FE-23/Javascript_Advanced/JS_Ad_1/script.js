/*Теоретичне питання
Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
1) У JavaScript кожен об'єкт має посилання на інший об'єкт, який називається "прототипом". Коли ви намагаєтесь звернутися до властивості або методу об'єкта і JavaScript не може знайти його безпосередньо в об'єкті, він буде шукати цю властивість або метод у прототипі цього об'єкта, кінцева точка undefined.
Для чого потрібно викликати super() у конструкторі класу-нащадка?
2)super() потрібний для наслідування консутруктора батька, або його зміни. 


Завдання
Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.*/

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        if (value.length < 4) {
            alert("Імя закоротке");
            return;
        }
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        if (value < 0) {
            alert("Ви помилились");
            return;
        }
        this._age = value;
    }

    get salary() {
        return this._salary;
    }
    
    set salary(value) {
        this._salary = value;
    }
}

const mar = new Employee("Maria", 20, 100);
console.log(mar.name);
mar.name = "qqq";
console.log(mar.name);


class Programmer extends Employee {
    constructor(name, age, salary, langs){
        super(name, age, salary);
        this._langs = langs;
    }
    get salary(){
        return this._salary * 3;
    }
    get langs() {
        return this._langs;
      }
};

const prog1 = new Programmer("John", 25, 50000, ["JavaScript", "Python"]);
const prog2 = new Programmer("Alice", 30, 60000);
const prog22 = new Programmer("John", 25, 50000, 4);

console.log(prog1.name, prog1.age, prog1.salary, prog1.langs);
console.log(prog2.name, prog2.age, prog2.salary, prog2.langs);
console.log(prog22.name, prog22.age, prog22.salary, prog22.langs);






