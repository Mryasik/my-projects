const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
    //  name: "Коти в мистецтві",
    }
  ];

const ARRAY_KEYS = ["author", "name", "price"];

const b = document.querySelector("body");
const mainDiv = document.createElement("div");
const list = document.createElement("ul");

mainDiv.id = "root";

b.append(mainDiv);
mainDiv.append(list);

function createLi(element) {
  li = document.createElement("li");
  li.innerText = (`${element}`);
  list.append(li);
}

function createErrorMessage(object) {
  let message = "";
  ARRAY_KEYS.forEach(element => {
    if (!object[element]) {
      message += `Немає ${element}; `
    };
  });
  // if (!object["author"]) {
  //   message += "Немає Автора" + " ";
  // } 
  // if (!object.name) {
  //   message += "Немає Назви" + " ";
  // } 
  // if (!object.price) {
  //   message += "Немає Ціни" + " ";
  // }
  return message;
}

  for (let i = 0; i < books.length; i++) {
    let message = createErrorMessage(books[i]);

    const error = new TypeError(message);

    try {
      if(!message){
        createLi(`${books[i].author}  ${books[i].name}  ${books[i].price}`);
      }else {
        throw error;
      }
      }
      catch(err) {
        console.error(err.message)
      }
  }

