class Card{
    constructor(id,name, email, title, post) {
        this.id = id
        this.name = name;
        this.email = email;
        this.title = title;
        this.post = post;
    }
    createDiv () {
        const mainPost = document.createElement("div");
        mainPost.className = "post";
        document.body.append(mainPost);

        const postName = document.createElement("div");
        postName.className = "name";
        mainPost.append(postName);

        const nameList = document.createElement("ul");
        nameList.className = "name-list";
        postName.append(nameList);
        nameList.insertAdjacentHTML("beforeend", `
        <li>${this.name}</li>
        <li>${this.email}</li>
        `)


        const postDescription = document.createElement("div");
        postDescription.className = "description";
        mainPost.append(postDescription);
        postDescription.insertAdjacentHTML("beforeend", `
        <h3>${this.title}</h3>
        <p>${this.post}</p>
        `)


        const postButtons = document.createElement("div");
        postButtons.className = "buttons";
        mainPost.append(postButtons);
        const deleteBtn = document.createElement("button");
        deleteBtn.className = "glow-on-hover";
        postButtons.append(deleteBtn)
        deleteBtn.append("DELETE")

        deleteBtn.addEventListener("click", () => {
            fetch("https://ajax.test-danit.com/api/json/posts/" + this.id, {
                method: 'DELETE',})
                .then(() => {
                    if (confirm("Really delete?")) {
                      mainPost.remove();
                    }
                  })
        })



        const editBtn = document.createElement("button");
        editBtn.className = "glow-on-hover";
        postButtons.append(editBtn)
        editBtn.append("EDIT")

        editBtn.addEventListener("click", () => {
            let newTitle = prompt("New Title");
            let newPost = prompt("New Post");
           
            if (newTitle !== null && newPost !== null) {
                fetch("https://ajax.test-danit.com/api/json/posts/" + this.id, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ title: newTitle, body: newPost }),
                })
                .then(response => response.json())
                .then(data => {
                    console.log('successfully:', data);
                })
                .catch(error => {
                    console.error(error);
                });
            }
            postDescription.innerHTML =
            `<h3>${newTitle}</h3>
             <p>${newPost}</p>`

        })
    }
}

fetch("https://ajax.test-danit.com/api/json/posts")
.then(res => res.json())
.then(posts => {


    fetch("https://ajax.test-danit.com/api/json/users")
    .then(res => res.json())
    .then(users => { 


        posts.forEach(post => {
        const {name, email} = users.find(el => el.id === post.userId)

            const newPost = new Card(post.id,name, email, post.title, post.body)
            newPost.createDiv()
    })

})
.catch(err => console.log(err))
})
.catch(err => console.log(err))


// ADD POST BUTTON

// const add = document.querySelector(".js-addPost")

// add.addEventListener("click", () => {
//     let addName = prompt("Your Name");
//     let addEmail = prompt("Your Email");
//     let addTitle = prompt("Your Title");
//     let addPost = prompt("Your Post");

//     fetch("https://ajax.test-danit.com/api/json/posts", {
//         method: 'POST',
//                 headers: {
//                     'Content-Type': 'application/json',
//                 },
//                 body: JSON.stringify({name: addName, email: addEmail, title: addTitle, body: addPost }),
//     })
//     new Card(1,addName,addEmail,addTitle,addPost)
// })
