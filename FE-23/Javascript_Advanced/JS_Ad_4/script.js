/* Теоретичне питання
Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

AJAX (Asynchronous JavaScript and XML) – технологія, що дозволяє 
здійснювати асинхронний обмін даними між клієнтом та сервером без потреби перезавантажувати сторінку.
*/

// Завдання

const container = document.querySelector(".container")
const loader = document.querySelector("#spiner");


fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(res => res.json())
    .then(film => {
        film.forEach(element => {
            let listFilm = document.createElement("ul");
            let loader = document.createElement("span");

            loader.classList.add("loader")

            container.append(listFilm);

            listFilm.insertAdjacentHTML("beforeEnd", "<h1>Film</h1>");
            listFilm.insertAdjacentHTML("beforeEnd", `<li> name: ${element.name} </li>`);
            listFilm.insertAdjacentHTML("beforeEnd", `<li> episodeId: ${element.episodeId} </li>`);
            listFilm.insertAdjacentHTML("beforeEnd", `<li> openingCrawl: ${element.openingCrawl} </li>`);
            
            listFilm.after(loader);

            element.characters.forEach(e => {

                fetch(e)
                .then(charac => charac.json())
                .then(chart => {

                    let listChar = document.createElement("ul");
                    listFilm.append(listChar);

                    listChar.insertAdjacentHTML("beforeBegin", "<h1>Character</h1>");
                    loader.classList.remove("loader");

                    for (let key in chart) {
                        listChar.insertAdjacentHTML("beforeBegin", `<li> ${key}: ${chart[key]} </li>`);  

                    }
                })
                .catch(err => {
                    console.log("Character Error:", err)
                })
            })
        });
    })
    .catch(err => {
        console.log("Film Error:", err)
    })