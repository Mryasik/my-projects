/*1. Як можна оголосити змінну у Javascript?
Щоб оголосити зміну в JS потрібно використовувати треба використовувати 
let const (var - застаріло)
Напрмклад

let x; Оголошення та ініціалізація
x = "Hello World"; // Присвоєння - процес записання значення в зміну(створення комірки з памятю)

Все в одному
let y = "Hello World";*/


/* 2. У чому різниця між функцією prompt та функцією confirm?

Функція prompt - виводить модальне окно де ми маємо строку де пишемо значення, 
кнопку ОК та кнопку Отмена
При незаповнені поля та ОК нам повертає пусту строку з типом string
При заповненому полі та ОК нам повертає значення з типом string
При заповненому/незаповненому полі та Отмена нам повертає Null з типом object

Функція confirm - виводить модальне окно з двома кнопками ОК/Отмена
і може повенути нам Boolean - OK-true / Отмена-false*/


/*3. Що таке неявне перетворення типів? Наведіть один приклад.
Неявне перетворення типів - це процес переведення одного типу даних в інший.

Переведення в String
1)let num = 25;
typeof(""+num);
2)num.toSring();
3)String(num);
4)`${num}`;

Переведення в Number
1)let str = "123"
Number(str)
2)(+str)
3)parseInt(str) округлює бех запятих
4)parseFloat(str) не округлює бех запятих*/


// Завдання 1
const name = "Yarik";
let admin = name;
console.log(admin)
// Завдання 2
let days = 5;
const secondInDay = 86400;
console.log(days*secondInDay);
// Завдання 3
let userName = prompt("Your Name");
console.log(userName);