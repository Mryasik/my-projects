/* Опишіть своїми словами як працює метод forEach.

1) forEach - переберає масив та виконує вказану нами функцію для кожного елементу масиву.

Як очистити масив?

2) Очистити масив можна

- Через переприсвоєння пустого масиву
arr = [] або arr = Array()
- Через свойство length
arr.length = 0
- Array.prototype.splice()
arr.splice(0,arr.length); .. arr.length=0;

Як можна перевірити, що та чи інша змінна є масивом?
3) за допомогою методу Array.isArray()
*/

// Завдання
// Реалізувати функцію фільтру масиву за вказаним типом даних. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].

let arr = ['hello', 'world', 23, '23', null];

let filterBy = function(array, dataType){
    let newarr = array.filter(elem => typeof(elem) !== dataType);
    return newarr;
}

let newarray = filterBy(arr, "string");

console.log(newarray);
console.log(arr);

