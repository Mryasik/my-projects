/*
Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
Прототип це місце зберігання властивостей або функцій обєкт. І це означає що всі переданні методи будуть доступні для обєктів. Якщо властивість або метод не знаходиться в самому об'єкті, пошук продовжується вгору ланцюжка прототипів до тих пір, поки не знайдено відповідний елемент або не досягнуто кінця ланцюжка, коли прототип - null.
Для чого потрібно викликати super() у конструкторі класу-нащадка?
Виклик super() у конструкторі класу-нащадка є необхідним для виклику конструктора батьківського класу. Це дозволяє ініціалізувати спадковані властивості та виконати будь-які додаткові дії, які можуть бути визначені в конструкторі батьківського класу.

Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
*/
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        if (value.length < 4) {
            alert("Імя закоротке");
            return;
        }
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        if (value < 0) {
            alert("Ви помилились");
            return;
        }
        this._age = value;
    }

    get salary() {
        return this._salary;
    }
    
    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee{
    constructor(_name, _age, _salary, lang){
        super(_name, _age, _salary);
        this._lang = lang;
    }
    get salary() {
        return this._salary = this._salary * 3;
    }

    get lang () {
        return this._langs;
    }
}


const alex = new Employee("Alex", 18, 1000);

console.log(alex);

const dima = new Programmer("Dima", 20, 1200, 3);

console.log((dima));

console.log(dima.salary);