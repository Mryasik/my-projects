/*
Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
try...catch можна використовувати при обробці операцій які можуть повернути поимлку та зламати наш код.
Також її можна використовувати при асинхроних функціях
*/
const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];


  const root = document.querySelector("#root");
  const ulList = document.createElement("ul");
  root.append(ulList);

  function createLi(value) {
    const li = document.createElement("li");
    li.innerText = value;
    ulList.append(li);
  }

  function createMessage(obj) {
    let message = "";
  
    if (!obj.author) {
      message += "Немає Автора";
    } 
    if (!obj.name) {
      message += "Немає Назви";
    } 
    if (!obj.price) {
      message += "Немає Ціни";
    }
  
    return message
  }
  
  books.forEach(book => {
    const resultMessage = createMessage(book);

    const error = new TypeError(resultMessage);


    try{
        if (!resultMessage) {
            Object.keys(book).forEach(key => {
          createLi(key + " - " + book[key]);
        });
      }else {
        throw error
      }
    }   
      catch(err) {
        console.error(err.message)
      }
    })