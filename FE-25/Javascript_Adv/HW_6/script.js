// Завдання 1
// Асинхронність у JavaScript означає, що програма може виконувати декілька завдань одночасно, не чекаючи завершення кожного перед переходом до наступного. Це корисно для виконання операцій, які можуть займати час, таких як завантаження даних з сервера чи робота з файлами.
// Завдання 2

const btn = document.querySelector(".btn")

console.log(btn);

const GET_IP ="https://api.ipify.org/?format=json";


// function getInfo() {
// fetch(GET_IP).then(res => res.json())
//     .then(data => {
//         console.log(data.ip)
//         const ip = data.ip
//         const GET_ADRESS = `https://ip-api.com/${ip}`;

//         fetch(GET_ADRESS)
//             .then(res => res.json())
//             .then(addressData => {
//                 const resultElement = document.getElementById('result');
//                 resultElement.innerHTML = `
//                 <p>Континент: ${addressData.continent}</p>
//                 <p>Країна: ${addressData.country}</p>
//                 <p>Регіон: ${addressData.regionName}</p>
//                 <p>Місто: ${addressData.city}</p>
//                 <p>Район: ${addressData.district}</p>`;
//             })
//             .catch(error => {
//                 console.error('Помилка під час виконання другого запиту:', error);
//             });
//     })
//     .catch(error => {
//         console.error('Помилка під час виконання першого запиту:', error);
//     });
// }

// btn.addEventListener("click",getInfo);


async function getInfoAsync() {
    try {
        const responseIP = await fetch(GET_IP);
        const data = await responseIP.json();
        console.log(data.ip);

        const ip = data.ip;
        const GET_ADDRESS = `https://ip-api.com/json/${ip}`;

        const responseAddress = await fetch(GET_ADDRESS);
        const addressData = await responseAddress.json();

        const resultElement = document.getElementById('result');
        resultElement.innerHTML = `
            <p>Континент: ${addressData.continent}</p>
            <p>Країна: ${addressData.country}</p>
            <p>Регіон: ${addressData.regionName}</p>
            <p>Місто: ${addressData.city}</p>
            <p>Район: ${addressData.district}</p>`;
    } catch (error) {
        console.error('Помилка під час виконання запиту:', error);
    }
}

btn.addEventListener("click",getInfoAsync);